#!/bin/bash

# Run this script from the root of the repo

# Compile any Sass changes
sass -s compressed assets/css/torahPresenter.scss assets/css/torahPresenter.css

# Resize and save all icon sizes (OS X)
sizes=(96 48 32 16)
for size in "${sizes[@]}"; do
    new_filename="icon@${size}.png"
    sips -Z "$size" "assets/img/icon@128.png" --out "assets/img/$new_filename"
done

# Package the unsigned Firefox plugin
zip -rj assets/firefox/torahPresenterUNSIGNED.xpi assets/firefox/manifest.json assets/js/torahPresenter.js assets/css/torahPresenter.css assets/css/torahPresenter.css.map assets/js/jquery-3.7.1.min.js assets/img/icon@48.png assets/img/icon@96.png

# Package the Chrome plugin (OS X)
# open -a "Google Chrome" --args --pack-extension=chrome --pack-extension-key=chrome/torahPresenter.pem
# zip -rj assets/chrome/torahPresenterUNSIGNED.zip assets/chrome/manifest.json assets/js/torahPresenter.js assets/css/torahPresenter.css assets/css/torahPresenter.css.map assets/js/jquery-3.7.1.min.js assets/img/icon@48.png assets/img/icon@96.png
