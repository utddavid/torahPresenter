$(function() {
  
  /* Add initial classes */
  $("body").addClass("fullscreen");
  $("table.Co_TanachTable:first-of-type .Co_Verse:first-of-type").addClass("active");
  
  /* Adjust top for #ParshaContent to account for #TorahReadingOption header */
  $(".fullscreen #ParshaContent").css("top",$("#TorahReadingOptions").innerHeight() + "px");
  
  /* The. Button. */
  $('#TorahReadingOptions .medium:first-child').prepend("<img src='https://icongr.am/fontawesome/unsorted.svg?size=16&color=105689' id='fullscreenBtn' />");
  $("#fullscreenBtn").click(function() {
    $("body").toggleClass("fullscreen");
    toggleRashi();
  });
  
  /* Clicky stuff */
  $(".fullscreen #ParshaContent .Co_Verse").click(function() {
    $(".active").removeClass("active");
    $(this).addClass("active");
    //$(this).siblings().removeClass("active");
    $(this).focus();
  });
  
  // Key up event
  $(document).on("keypress", function(e) { 
    // Variables
    var window_scroll = $('.fullscreen #ParshaContent').scrollTop();  // Amount that #ParshaContent has been scrolled
    var active_top = $(".Co_Verse.active").offset().top;              // Active verse from top of screen
    var window_h  = $(window).innerHeight();                          // Inner height of browser window
    var header_h   = $("#TorahReadingOptions").outerHeight();              // Height of header (to account when scrolling under)
    
    if (e.which == "106") { // 106 = j
      // Check for next verse
      var next_verse = $(".Co_Verse.active").nextAll(".Co_Verse").first();
      console.log("next_verse.length: " + next_verse.length);
      
      // If last verse in chapter
      if(next_verse.length <= 0) {
        console.log("There is no next verse 😲");
        //$(".Co_Verse.active").removeClass("active").parents("table").nextAll("table").first().find(".Co_Verse").first().click();
        next_verse = $(".Co_Verse.active").removeClass("active").parents("table").nextAll("table").first().find(".Co_Verse").first();
      }
      
      // Click it
      next_verse.click();
      
      // Update variables
      var active_top = $(".Co_Verse.active").offset().top;              // Active verse from top of screen
      
      // Scroll to the verse
      if(active_top > window_h - window_h / 3 || active_top < 0) {
        $('.fullscreen #ParshaContent').stop().animate({scrollTop: active_top + window_scroll - header_h - 25}, 2000);
      }
      console.log("header_h: " + header_h + ", active_top: " + active_top + ", window_scroll: " + window_scroll);
    }
    
    if (e.which == "107") { // 107 = k
      // Check for prev verse
      var prev_verse = $(".Co_Verse.active").prevAll(".Co_Verse").first();
      console.log("prev_verse.length: " + prev_verse.length);
      
      // If first verse in chapter
      if(prev_verse.length <= 0) {
        console.log("There is no prev verse 😲");
        prev_verse = $(".Co_Verse.active").removeClass("active").parents("table").prevAll("table").first().find(".Co_Verse").last();
      }
      
      // Click it
      prev_verse.click();
      
      // Update variables
      var active_top = $(".Co_Verse.active").offset().top;              // Active verse from top of screen
      
      // Scroll to the verse
      if(active_top > window_h - window_h - header_h || active_top < 0) {
        $('.fullscreen #ParshaContent').stop().animate({scrollTop: active_top + window_scroll - header_h - 25}, 2000);
      }
      console.log("header_h: " + header_h + ", active_top: " + active_top + ", window_scroll: " + window_scroll);
    }
    
    
    if (e.which == "104") { // 104 = h
      $("#TorahReadingOptions .co_toggle_buttons label.selected").prev("label").click();
    }
    if (e.which == "108") { // 108 = l
      $("#TorahReadingOptions .co_toggle_buttons label.selected").next("label").click();
    }
  });
  
  // Special Rashi stuff
  toggleRashi();
  
  $("#ShowRashiLink").click(function() {
    toggleRashi();
  });
  
  function toggleRashi() {
    if($("#ShowRashiLink").attr("value") == "true") {
      $(".Co_Rashi").show();
    } else {
      $(".Co_Rashi").hide();
    }
  }
  
});