# Torah Presenter for group reading

This is a browser plugin that adds a presentation view and interface for the 
Torah readings on popular websites like chabad.org

![Torah Presenter Screenshot GIF](assets/img/torahPresenter.gif)

I wanted a way to share my screen to a projector and have everyone in the room
be able to easily follow along during our group reading time of the weekly parsha.
This plugin will isolate and enlarge the text and highlight the current verse.

You can click on the verse you want to highlight and use short-keys to move the
highlighted verse. You can press 'k' for up and 'j' for down. You can also use 
'l' to move to the previous portion and ';' to move to the next portion.

You can download the latest plugin for [Firefox](versions/firefox/) from this repo.

## Dev requirements

You can run the [buildPlugins](buildPlugins.sh) script to build the plugins
but this will assume that you have [Sass installed](https://sass-lang.com/install/) 
and available in your $path. Because of this, please don't edit css files but
only edit their respective scss file.

All the @ icons are generated in the buildPlugins script from the icon@128 which 
is saved from the psd.

## To-Dos and future improvements

- [x] Add the app icons
- [ ] Add cookie to remember if user toggled presenter mode or not
- [ ] List the plugin in the Firefox plugin directory
- [ ] The plan is to have this working for other sites as well, like:
  - [ ] aish.com
  - [ ] sefaria.org
- [ ] I'd like to create a Chrome extension
- [ ] Create a plugin settings page with configurable options
- [ ] Fix the bugs:
  - [ ] Stop the highlighting from moving past the first or last verse
  - [ ] Enable highlighting to move properly on the 'complete' tab
  - [ ] Use different navigation buttons. Preferably configurable.